import unittest
import os
import logging
import tarfile
import io

from unittest.mock import patch, Mock
from os.path import dirname, join, abspath, isdir, exists
from copy import deepcopy
from pyori import (
    read_all_goal_files, merge_goals, CSV_HEADER, moving_sum, variance,
    validate_project_paths, _main, read_goal_file, write_merged_tasks,
    )
from tempfile import TemporaryDirectory
from datetime import datetime


TEST_DIR = dirname(abspath(__file__))  # root is where this file is located

TEST_DATA_DIR = abspath(join(TEST_DIR, 'data'))


class TestPyori(unittest.TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def tearDown(self):
        logging.disable(logging.NOTSET)

    def test_01_read_all_goal_files(self):

        goals_path = join(TEST_DATA_DIR, 'goals_01')
        data = read_all_goal_files(goals_path)
        expected = [
            {
                'id': 'goal_01',
                'name': 'goal_01',
                'description': '',
                'depends': [],
                'ignore': False,
                'tasks': [
                    {'task': 'task_01', 'score': 1, 'done': 'no'},
                    {'task': 'task_02', 'score': 2, 'done': 'no'},
                    {'task': 'task_03', 'score': 5, 'done': 'no'},
                    {'task': 'task_04', 'score': 2, 'done': 'no'},
                    {'task': 'task_05', 'score': 3, 'done': 'no'},
                    {'task': 'task_06', 'score': 2, 'done': 'no'},
                    {'task': 'task_07', 'score': 1, 'done': 'no'},
                    {'task': 'task_08', 'score': 2, 'done': 'no'},
                    ],
                },
            {
                'id': 'goal_02',
                'name': 'goal_02',
                'description': '',
                'depends': [],
                'ignore': False,
                'tasks': [
                    {'task': 'task_01', 'score': 3, 'done': 'yes'},
                    {'task': 'task_02', 'score': 5, 'done': 'yes'},
                    {'task': 'task_03', 'score': 3, 'done': 'no'},
                    {'task': 'task_04', 'score': 1, 'done': 'no'},
                    {'task': 'task_05', 'score': 2, 'done': 'no'},
                    ],
                },
            {
                'id': 'goal_03',
                'name': 'goal_03',
                'description': '',
                'depends': [],
                'ignore': False,
                'tasks': [
                    {'task': 'task_01', 'score': 2, 'done': 'yes'},
                    {'task': 'task_02', 'score': 1, 'done': 'yes'},
                    {'task': 'task_03', 'score': 5, 'done': 'no'},
                    {'task': 'task_04', 'score': 1, 'done': 'yes'},
                    {'task': 'task_05', 'score': 3, 'done': 'no'},
                    {'task': 'task_06', 'score': 2, 'done': 'yes'},
                    {'task': 'task_07', 'score': 1, 'done': 'no'},
                    {'task': 'task_08', 'score': 2, 'done': 'no'},
                    {'task': 'task_07', 'score': 2, 'done': 'no'},
                    {'task': 'task_08', 'score': 3, 'done': 'no'},
                    ],
                }
            ]
        self.assertEqual(data, expected)

    def test_02_merge_goals(self):

        goals_path = join(TEST_DATA_DIR, 'goals_01')
        data = read_all_goal_files(goals_path)
        coeffs = {d['id']: 1 for d in data}
        merged = merge_goals(data, coeffs)

        self.assertEqual(type(merged), list)
        self.assertEqual(len(merged), 20)
        expected_keys = {'goal', *CSV_HEADER}  # NOQA
        for i, row in enumerate(merged):
            with self.subTest(i=i):
                if len(row) == len(expected_keys):
                    self.assertEqual(row.keys(), expected_keys)
                else:
                    self.assertEqual(row.keys(), {'goal', 'task'})
                    self.assertEqual(row['task'], 'COMPLETE')

    def test_03_moving_sum(self):

        out = moving_sum([], 1)
        self.assertEqual(out, [])

        out = moving_sum([], 5)
        self.assertEqual(out, [])

        out = moving_sum([1], 1)
        self.assertEqual(out, [1])

        out = moving_sum([1, 2, 3], 1)
        self.assertEqual(out, [1, 2, 3])

        out = moving_sum([1], 5)
        self.assertEqual(out, [])

        out = moving_sum([1, 2, 3, 4], 5)
        self.assertEqual(out, [])

        out = moving_sum([1, 2, 3, 4, 5], 5)
        self.assertEqual(out, [15])

        out = moving_sum([1, 2, 3, 4, 5, 6, 7, 8, 9], 3)
        self.assertEqual(out, [6, 9, 12, 15, 18, 21, 24])

    def test_04_variance(self):

        out = variance([])
        self.assertEqual(out, 0)

        out = variance([0])
        self.assertEqual(out, 0)

        out = variance([1])
        self.assertEqual(out, 0)

        out = variance([1, 2, 3, 4])
        self.assertEqual(out, 1.25)

        out = variance([-1.5, 0.5, 2.5, 0.5])
        self.assertEqual(out, 2.0)

    def test_05_merge_goals_uses_data_copy(self):

        goals_path = join(TEST_DATA_DIR, 'goals_01')
        data = read_all_goal_files(goals_path)
        data_orig = deepcopy(data)
        coeffs = {d['id']: 1 for d in data}
        merge_goals(data, coeffs)
        self.assertEqual(data, data_orig)

    def test_06_validate_project_paths(self):

        with TemporaryDirectory() as project_path:

            with self.assertRaises(ValueError):
                validate_project_paths(project_path)
            project_path, goals_path = validate_project_paths(
                project_path, make_project=True)
            self.assertTrue(isdir(project_path))
            self.assertTrue(isdir(goals_path))

    def test_07_auto_make_project(self):

        with TemporaryDirectory() as project_path:

            goals_path = join(project_path, 'goals')
            with self.assertRaises(ValueError):
                _main(project_path)
            _main(project_path, make_project=True)
            self.assertTrue(isdir(project_path))
            self.assertTrue(isdir(goals_path))

    def test_08_archive_goals(self):

        with TemporaryDirectory() as project_path:

            _main(project_path, make_project=True)
            archives_path = join(project_path, 'archives')
            self.assertFalse(exists(archives_path))
            with patch('pyori.datetime') as mock_datetime:
                mock_datetime.now = lambda: datetime.utcfromtimestamp(0)
                _main(project_path, make_archive=True)
            self.assertTrue(isdir(archives_path))
            tar_name = '1970-01-01_00-00-00.tar.gz'
            self.assertEqual(os.listdir(archives_path), [tar_name])
            with tarfile.open(join(archives_path, tar_name), 'r:gz') as tar:
                names = [x.name for x in tar]
            self.assertEqual(names, ['goals'])

    def test_09_csv_with_invalid_commas(self):

        path = join(TEST_DATA_DIR, 'tasks_with_commas_invalid.csv')
        with self.assertRaises(ValueError):
            read_goal_file(path)

    def test_10_csv_with_valid_commas(self):

        path = join(TEST_DATA_DIR, 'tasks_with_commas_valid.csv')
        out = read_goal_file(path)
        expected = [
            {'task': 'this, that, and the other', 'score': 1, 'done': 'no'},
            {'task': 'we should do 1, 2, and 3', 'score': 2, 'done': 'no'},
            {'task': 'just a regular task', 'score': 5, 'done': 'no'},
            ]
        self.assertEqual(out, expected)

    def test_11_write_csv_with_commas(self):

        merged = [
            {'goal': 'mygoal',
                'task': 'this, that, and the other', 'score': 1, 'done': 'no'},
            {'goal': 'mygoal',
                'task': 'we should do 1, 2, and 3', 'score': 2, 'done': 'no'},
            {'goal': 'mygoal',
                'task': 'just a regular task', 'score': 5, 'done': 'no'},
            ]
        s = io.StringIO()
        write_merged_tasks(s, merged)
        out = s.getvalue()
        expected = (
            'goal,task,score,done\r\n'
            'mygoal,"this, that, and the other",1,no\r\n'
            'mygoal,"we should do 1, 2, and 3",2,no\r\n'
            'mygoal,just a regular task,5,no\r\n'
            )
        self.assertEqual(out, expected)

    def test_12_read_all_goal_files_with_info(self):

        goals_path = join(TEST_DATA_DIR, 'goals_02')
        data = read_all_goal_files(goals_path)
        expected = [
            {
                'id': 'goal_03',
                'name': 'Big Goal #3',
                'description': '',
                'depends': [],
                'ignore': False,
                'tasks': [
                    {'task': 'task_01', 'score': 2, 'done': 'yes'},
                    {'task': 'task_02', 'score': 1, 'done': 'no'},
                    ],
                },
            {
                'id': 'goal_01',
                'name': 'Goal #1',
                'description': 'A description of things.',
                'depends': ['this', 'that', 'other'],
                'ignore': False,
                'tasks': [
                    {'task': 'task_01', 'score': 1, 'done': 'no'},
                    {'task': 'task_02', 'score': 2, 'done': 'no'},
                    {'task': 'task_03', 'score': 5, 'done': 'no'},
                    ],
                },
            {
                'id': 'goal_02',
                'name': 'goal_02',
                'description': '',
                'depends': [],
                'ignore': False,
                'tasks': [
                    {'task': 'task_01', 'score': 2, 'done': 'no'},
                    {'task': 'task_02', 'score': 3, 'done': 'no'},
                    ],
                },
            ]
        self.assertEqual(data, expected)

    def test_13_merge_goals_with_custom_names(self):

        goals_path = join(TEST_DATA_DIR, 'goals_02')
        data = read_all_goal_files(goals_path)
        coeffs = {d['id']: 1 for d in data}
        merged = merge_goals(data, coeffs)

        self.assertEqual(len(merged), 9)

        merged = sorted(merged, key=lambda x: x['goal'])
        expected = [
            {'goal': 'Big Goal #3',
                'task': 'task_02', 'score': 1, 'done': 'no'},
            {'goal': 'Big Goal #3', 'task': 'COMPLETE'},
            {'goal': 'Goal #1', 'task': 'task_01', 'score': 1, 'done': 'no'},
            {'goal': 'Goal #1', 'task': 'task_02', 'score': 2, 'done': 'no'},
            {'goal': 'Goal #1', 'task': 'task_03', 'score': 5, 'done': 'no'},
            {'goal': 'Goal #1', 'task': 'COMPLETE'},
            {'goal': 'goal_02', 'task': 'task_01', 'score': 2, 'done': 'no'},
            {'goal': 'goal_02', 'task': 'task_02', 'score': 3, 'done': 'no'},
            {'goal': 'goal_02', 'task': 'COMPLETE'},
            ]
        self.assertEqual(merged, expected)

    def test_14_merge_goals_with_dependencies(self):

        goals_path = join(TEST_DATA_DIR, 'goals_03')
        data = read_all_goal_files(goals_path)
        coeffs = {
            'goal_01': 1,
            'goal_02': 100000,  # normally would cause 2nd goal to come first
            }
        merged = merge_goals(data, coeffs)
        expected = [
            {'goal': 'Goal #1', 'task': 'task_01', 'score': 1, 'done': 'no'},
            {'goal': 'Goal #1', 'task': 'task_02', 'score': 1, 'done': 'no'},
            {'goal': 'Goal #1', 'task': 'task_03', 'score': 1, 'done': 'no'},
            {'goal': 'Goal #1', 'task': 'task_04', 'score': 1, 'done': 'no'},
            {'goal': 'Goal #1', 'task': 'task_05', 'score': 1, 'done': 'no'},
            {'goal': 'Goal #1', 'task': 'COMPLETE'},
            {'goal': 'goal_02', 'task': 'task_01', 'score': 1, 'done': 'no'},
            {'goal': 'goal_02', 'task': 'task_02', 'score': 1, 'done': 'no'},
            {'goal': 'goal_02', 'task': 'task_03', 'score': 1, 'done': 'no'},
            {'goal': 'goal_02', 'task': 'task_04', 'score': 1, 'done': 'no'},
            {'goal': 'goal_02', 'task': 'task_05', 'score': 1, 'done': 'no'},
            {'goal': 'goal_02', 'task': 'COMPLETE'},
            ]

        # assert a few times
        for i in range(5):
            with self.subTest(i=i):
                self.assertEqual(merged, expected)

    def test_15_merge_goals_with_circularities(self):

        goals_path = join(TEST_DATA_DIR, 'goals_04')
        data = read_all_goal_files(goals_path)

        coeffs = {'goal_01': 1, 'goal_02': 1}
        with patch('pyori.warning') as mock_warn:
            merge_goals(data, coeffs)
            self.assertEqual(mock_warn.call_count, 1)

        coeffs = {'goal_01': 0, 'goal_02': 1}  # goal_01 ignored
        with patch('pyori.warning') as mock_warn:
            self.assertEqual(mock_warn.call_count, 0)
            merge_goals(data, coeffs)
            self.assertEqual(mock_warn.call_count, 0)

        coeffs = {'goal_01': 1, 'goal_02': 0}  # goal_02 ignored
        with patch('pyori.warning') as mock_warn:
            self.assertEqual(mock_warn.call_count, 0)
            merge_goals(data, coeffs)
            self.assertEqual(mock_warn.call_count, 0)

    def test_16_merge_goals_some_ignored(self):

        goals_path = join(TEST_DATA_DIR, 'goals_05')
        data = read_all_goal_files(goals_path)
        coeffs = {d['id']: 1 for d in data}
        merged = merge_goals(data, coeffs)

        self.assertEqual(len(merged), 12)

        merged = sorted(merged, key=lambda x: x['goal'])
        expected = [
            {'goal': 'goal_01', 'task': 'task_01', 'score': 1, 'done': 'no'},
            {'goal': 'goal_01', 'task': 'task_02', 'score': 1, 'done': 'no'},
            {'goal': 'goal_01', 'task': 'task_03', 'score': 1, 'done': 'no'},
            {'goal': 'goal_01', 'task': 'task_04', 'score': 1, 'done': 'no'},
            {'goal': 'goal_01', 'task': 'task_05', 'score': 1, 'done': 'no'},
            {'goal': 'goal_01', 'task': 'COMPLETE'},
            {'goal': 'goal_03', 'task': 'task_01', 'score': 1, 'done': 'no'},
            {'goal': 'goal_03', 'task': 'task_02', 'score': 1, 'done': 'no'},
            {'goal': 'goal_03', 'task': 'task_03', 'score': 1, 'done': 'no'},
            {'goal': 'goal_03', 'task': 'task_04', 'score': 1, 'done': 'no'},
            {'goal': 'goal_03', 'task': 'task_05', 'score': 1, 'done': 'no'},
            {'goal': 'goal_03', 'task': 'COMPLETE'},
            ]
        self.assertEqual(merged, expected)
