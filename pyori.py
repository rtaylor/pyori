#!/usr/bin/env python

"""Usage: pyori [options]

Combine two or more prioritized goal sets into a single TODO list.

Options:
  -h --help                 display help
  -p --project-dir PATH     path to project directory (current working
                            directory by default)
  -a --archive              archive the project's current goals folder
  -i --interactive          launch interactive command-line interpreter
  --new-project             make a new, empty project
  -v --verbose              print all debug information and tracebacks
  --version                 show version

"""

import sys
import os
import csv
import logging
import logging.handlers
import tarfile
import cmd
import yaml

from docopt import docopt
from os.path import (
    join, isdir, splitext, exists, realpath,
    expanduser, basename, dirname,
    )
from random import choices
from datetime import datetime
from copy import deepcopy
from distutils.util import strtobool


VERSION = '0.2'

CSV_HEADER = ('task', 'score', 'done')

DONE_NEG = 'no'

DONE_POS = 'yes'


# logging methods
logger = logging.getLogger(__name__)
debug = logger.debug
warning = logger.warning
error = logger.error


def get_version():
    """Return the project's version string.
    """
    return VERSION


def clean_path(path):
    """Return the path in a normalized form.
    """
    return realpath(expanduser(path))


def log_exception(exc):
    """Log the given exception as an error and include the traceback
    as a debug message.
    """
    debug('An exception occured.', exc_info=True)
    error(exc)


def read_goal_file(file_path):
    """Return the parsed contents of the given goal file.
    """
    with open(file_path, 'r', newline='') as f:
        reader = csv.DictReader(f, skipinitialspace=True)
        data = []
        for i, row in enumerate(reader):
            try:
                if tuple(row.keys()) != CSV_HEADER:
                    raise ValueError(
                        'row does not match expected header ({})'
                        .format(', '.join(CSV_HEADER)))
                for k, v in row.items():
                    row[k] = v.strip()
                if len(row['task']) == 0:
                    raise ValueError('empty task name')
                try:
                    row['score'] = int(row['score'])
                except ValueError:
                    raise ValueError('invalid integer for score')
                if row['score'] <= 0:
                    raise ValueError('non-positive score')
                row['done'] = row['done'].lower()
                if row['done'] not in (DONE_POS, DONE_NEG):
                    raise ValueError(
                        '"done" value must be "{}" or "{}".'
                        .format(DONE_POS, DONE_NEG))
            except Exception as exc:
                raise ValueError(
                    '{} (line {}): {}'.format(file_path, i+2, str(exc)))
            data.append(dict(row))
        return data


def read_info_file(file_path):
    """Return the parsed contents of the given goal info file.
    """
    with open(file_path, 'r') as f:
        info = yaml.load(f)
    if type(info) is not dict:
        return None

    def raise_exc(key):
        raise TypeError('Invalid {} in info file: {}'.format(key, file_path))

    if type(info.get('name', '')) is not str:
        raise_exc('name')
    if type(info.get('description', '')) is not str:
        raise_exc('description')
    depends = info.get('depends', [])
    if type(depends) is not list:
        raise_exc('dependencies')
    if len(depends) > 0 and type(depends[0]) is not str:
        raise_exc('dependencies')
    if type(info.get('ignore', False)) is not bool:
        try:
            info['ignore'] = bool(strtobool(info['ignore']))
        except Exception:
            raise_exc('ignore')

    return info


def read_all_goal_files(goals_path):
    """Return a dictionary of all goal file contents found within the
    given directory.
    """
    if not isdir(goals_path):
        raise ValueError('Invalid path to goals directory: ' + goals_path)
    seen = set()
    data = []
    for root, dirs, files in os.walk(goals_path):
        for path in files:
            name, ext = splitext(path.lower())
            if ext == '.csv':
                if name in seen:
                    raise Exception(
                        'Two or more goals have the same name: "{}".'
                        .format(name))
                seen.add(name)
                full_path = join(root, path)
                debug('Reading goal file: ' + full_path)
                d = {'id': name, 'tasks': read_goal_file(full_path)}
                info_path = join(root, name + '.yaml')
                try:
                    goal_info = read_info_file(info_path) or {}
                    debug('Info file found: ' + info_path)
                except FileNotFoundError:
                    goal_info = {}
                d['name'] = goal_info.get('name', name)
                d['description'] = goal_info.get('description', '')
                d['depends'] = goal_info.get('depends', [])
                d['ignore'] = goal_info.get('ignore', False)
                data.append(d)
    return sorted(data, key=lambda d: d['name'])


def write_merged_tasks(f, data):
    """Write list of merged tasks to the given file descriptor in CSV format.
    """
    if len(data) == 0:
        return
    fieldnames = ('goal', *CSV_HEADER)  # NOQA
    writer = csv.DictWriter(f, fieldnames)
    writer.writeheader()
    for row in data:
        writer.writerow(row)


def validate_project_paths(project_path, make_project=False):
    """Validate and return the project path with its subfolders.
    """
    project_path = clean_path(project_path)
    debug('Project path: ' + project_path)
    if not exists(project_path) and make_project:
        warning('Making project directory: ' + project_path)
        try:
            os.mkdir(project_path)
        except FileNotFoundError:
            pass
    if not isdir(project_path):
        raise ValueError('Invalid project directory: ' + project_path)
    goals_path = join(project_path, 'goals')
    if not exists(goals_path) and make_project:
        warning('Making goals directory: ' + goals_path)
        os.mkdir(goals_path)
    if not isdir(goals_path):
        raise ValueError(
            'Project path does not contain a goals directory: ' + goals_path)
    return project_path, goals_path


def validate_output_path(output_path):
    """Validate and return the given output file path.
    """
    output_path = clean_path(output_path)
    debug('Output path: ' + output_path)
    if not exists(dirname(output_path)):
        raise ValueError('Invalid output file path: ' + output_path)
    return output_path


def get_archives_path(project_path):
    """Return the validated path to the archives folder, creating the
    folder if it does not already exist.
    """
    assert isdir(project_path)
    archives_path = join(project_path, 'archives')
    if not exists(archives_path):
        warning('Making archives directory: ' + archives_path)
        os.mkdir(archives_path)
    if not isdir(archives_path):
        raise ValueError(
            'Invalid archives directory: ' + archives_path)
    return archives_path


def create_archive(dst_path, src_path):
    """Tarball and compress the source path.
    """
    assert dst_path.endswith('.tar.gz')
    debug('Creating a new archive: ' + dst_path)
    with tarfile.open(dst_path, 'w:gz') as tar:
        debug('Adding source to archive: ' + src_path)
        arcname = basename(src_path)
        assert len(arcname) > 0
        tar.add(src_path, arcname=arcname)


def moving_sum(sequence, n):
    """Return the moving sum for the given sequence and window size.
    """
    assert n > 0
    return [sum(sequence[i-n:i]) for i in range(n, len(sequence)+1)]


def variance(sequence):
    """Return the variance of the sequence (complete sample).
    """
    if len(sequence) == 0:
        return 0
    mean = sum(sequence) / len(sequence)
    var = sum([(x - mean)**2 for x in sequence]) / len(sequence)
    return var


def merge_goals(goal_data, importance_coeffs):
    """Randomly merge the given lists of goal tasks according to the given
    importance coefficients (non-negative integers mapped to each goal).
    """
    assert type(goal_data) is list
    assert type(importance_coeffs) is dict
    assert len(goal_data) == len(importance_coeffs)

    # importance coeffs, indices, and rolling weights for each channel
    coeffs = [importance_coeffs[d['id']] for d in goal_data]
    indices = list(range(len(goal_data)))
    weights = [0 for c in goal_data]

    # convert goal data to list of task queues, ignoring "done" tasks
    def filter_tasks(tasks):
        return list(filter(lambda t: t['done'] == DONE_NEG, tasks))
    channels = [filter_tasks(d['tasks']) for d in goal_data]

    # list of dependencies for each channel
    _ids = {d['id'] for d in goal_data}
    dependencies = []
    for d in goal_data:
        deps = set()
        for x in d['depends']:
            if x in _ids:
                deps.add(x)
            else:
                warning('Ignoring "{}" dependency: "{}"'.format(d['id'], x))
        dependencies.append(deps)

    # treat ignored goals as "complete"
    completed_goals = {
        d['id'] for i, d in enumerate(goal_data)
        if (len(channels[i]) == 0) or (coeffs[i] == 0) or d['ignore']}

    # merge the channels
    merged = []
    while True:
        for i, tasks in enumerate(channels):
            c = coeffs[i]
            if goal_data[i]['id'] in completed_goals:
                weights[i] = 0
            elif len(dependencies[i] - completed_goals) == 0:
                # dependencies are met, so increment the probability
                weights[i] += c
        if len(completed_goals) == len(channels):
            break
        elif set(weights) == {0}:
            warning('Aborting merge process due to circular dependencies.')
            break
        idx = choices(indices, weights=weights, k=1)[0]
        weights[idx] -= coeffs[idx]
        goal_id = goal_data[idx]['id']
        goal_name = goal_data[idx]['name']
        tasks = channels[idx]
        merged.append({'goal': goal_name, **tasks.pop(0)})
        if len(tasks) == 0:
            merged.append({'goal': goal_name, 'task': 'COMPLETE'})
            completed_goals.add(goal_id)

    return merged


def _main(
        project_path,
        make_project=False,
        make_archive=False,
        importance_coeffs=None,
        output_path=None):
    """Execute program.
    """
    debug('Starting program execution.')

    # determine paths
    project_path, goals_path = validate_project_paths(
        project_path, make_project=make_project)

    # validate output path if given
    if output_path is not None:
        output_path = validate_output_path(output_path)

    # optionally make archive and return
    if make_archive:
        debug('Archiving the goals directory.')
        archives_path = get_archives_path(project_path)
        now = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        tar_path = join(archives_path, now + '.tar.gz')
        create_archive(tar_path, goals_path)
        return

    # read goals from files
    data = read_all_goal_files(goals_path)

    # find the best way to merge the goal tasks into single list
    debug('Merging tasks from {} goals.'.format(len(data)))
    coeffs = {d['id']: 1 for d in data}
    if importance_coeffs:
        coeffs.update(importance_coeffs)
    best = None
    best_var = None
    for i in range(20):
        merged = merge_goals(data, coeffs)
        merged_scores = [x['score'] for x in merged if 'score' in x]
        merged_var = variance(moving_sum(merged_scores, 5))
        if best is None or merged_var < best_var:
            best = merged
            best_var = merged_var
    merged = best

    # write the merged tasks to file or stdout
    debug('Writing merged tasks to {}.'.format(output_path or 'stdout'))
    if output_path:
        if exists(output_path):
            warning('Overwriting file: ' + output_path)
        with open(output_path, 'w') as f:
            write_merged_tasks(f, merged)
    else:
        write_merged_tasks(sys.stdout, merged)

    debug('Program execution complete.')


class PyoriShell(cmd.Cmd):

    intro = 'Type help or ? to list commands. CTRL-C to quit.\n'

    prompt = 'pyori> '

    def __init__(self, project_path, *args, **kwargs):
        self._project_path = project_path
        self._importance_coeffs = None
        super().__init__(*args, **kwargs)

    def cmdloop(self, *args, **kwargs):
        """Override.
        """
        try:
            return super().cmdloop(*args, **kwargs)
        except KeyboardInterrupt:
            print('')

    def onecmd(self, line):
        """Override.
        """
        try:
            return super().onecmd(line)
        except Exception as exc:
            log_exception(exc)

    def emptyline(self):
        """Override.
        """
        return

    def get_names(self):
        """Override.
        """
        names = super().get_names()
        return [n for n in names if n != 'do_EOF']

    def do_EOF(self, arg):
        "Quit the command-line interpreter."

        print()
        return True

    def do_quit(self, arg):
        "Quit the command-line interpreter."

        return True

    def _validate_project_paths(self, path=None):
        """Validate a given project path and to prompt user
        to create project if it does not exist.
        """
        path = path or self._project_path
        try:
            return validate_project_paths(path)
        except ValueError as exc:
            prompt = (
                'Invalid project path ({}). '
                'Make project [y/N]? '.format(path))
            response = input(prompt) or 'n'
            make_project = response.lower() in ('y', 'yes')
            if make_project:
                return validate_project_paths(path, True)
            raise exc

    def do_path(self, arg):
        "Get or set the project path."

        path = arg.strip()
        if path:
            self._project_path = self._validate_project_paths(path)[0]
        print('Project:', self._project_path)

    def _get_goals(self):
        """Return all goal data for the project."""

        goals_path = self._validate_project_paths()[1]
        return read_all_goal_files(goals_path)

    def do_list(self, arg):
        "List all goals."

        print('\nGoals\n=====')
        for d in self._get_goals():
            print(d['name'])
        print()

    def do_coeffs(self, arg):
        "Set importance coefficients for each goal."

        coeffs = {}
        default = 1
        for d in self._get_goals():
            id_ = d['id']
            name = d['name']
            prompt = 'Goal "{}" [{}]: '.format(name, str(default))
            c = input(prompt) or default
            try:
                c = int(c)
                if c < 0:
                    raise ValueError
            except ValueError:
                raise ValueError(
                    'Importance coefficient must be a non-negative integer.')
            coeffs[id_] = c
        self._importance_coeffs = coeffs

    def do_merge(self, arg):
        ("Merge the goals into a single list. Optionally provide a "
            "file path to print the output.")

        project_path = self._validate_project_paths()[0]
        kwargs = {'importance_coeffs': self._importance_coeffs}
        output_path = arg.strip()
        if output_path:
            kwargs['output_path'] = output_path
        _main(project_path, **kwargs)
        print('Merge complete.' if output_path else '')

    def do_archive(self, arg):
        "Archive the current goal set."
        project_path = self._validate_project_paths()[0]
        _main(project_path, make_archive=True)
        print('Archive created.')


def main():
    """Main entry point for program.
    """

    # parse user arguments
    options = docopt(__doc__, version=get_version())

    # logger captures all levels
    logger.setLevel(logging.DEBUG)

    # log handler that prints to screen
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG if options['--verbose'] else logging.WARNING)
    ch.setFormatter(logging.Formatter('%(levelname)s: %(message)s'))
    logger.addHandler(ch)

    # validate program arguments
    project_path = options['--project-dir'] or os.getcwd()
    make_project = options['--new-project']
    make_archive = options['--archive']
    interactive = options['--interactive']

    # execute program
    try:
        if interactive:
            PyoriShell(project_path).cmdloop()
        else:
            _main(
                project_path,
                make_project=make_project,
                make_archive=make_archive,
                )
        return 0
    except Exception as e:
        log_exception(e)
        return 1


if __name__ == '__main__':
    sys.exit(main())
