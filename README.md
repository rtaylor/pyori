pyori
=====

A Python (3.6) script to combine two or more prioritized goal sets into a single *TODO* list. Developed on and for Linux (Ubuntu 16.04), but should be cross-platform compatible.


Installation
------------

To install the most recent version:

	sudo pip3 install docopt pyyaml
	sudo pip3 install git+https://rtaylor@bitbucket.org/rtaylor/pyori.git

To install the development branch, append `@develop` to the command above.


Development Setup
-----------------

1. Download or clone the project:

		git clone https://rtaylor@bitbucket.org/rtaylor/pyori.git
		cd pyori
		git checkout develop

2. Setup and activate a Python 3.6 virtual environment:

		make install-venv

3. Run the tests and try building the project in-place:

		make test
		make test-build


Introduction
------------

Pyori is a small management tool for people who work on multiple projects, with multiple *TODO* lists, yet wish that they were only working from one *TODO* list. Pyori conveniently merges a set of task lists into a single "smart" list. When working from the "smart" list, one avoids the decision fatigue associated with repeatedly choosing the next task to work on from multiple projects, while ensuring that progress is always made towards all project objectives.

In Pyori parlance, the "task lists" are called *goal files*, or simply *goals*. Goals are basically CSV files, each of which list any number of discrete tasks that are required to complete some defined objective. A goal can be whatever you want it to be, as long as it can be discretized into a sequence of tasks.

Each line in a goal file represents a single, discrete task. The task has a column for its **name**, a column for an integer **score**, and a column for marking that task as **done**. The scores are arbitrary numbers representing the *difficulty* or *time requirement* of each task relative to the others tasks. Higher scores mean more time or effort required to complete the task.

Additional information about a goal can be recorded in a `*.yaml` file of the same name. This optional file might define things like the goal's proper name, description, dependencies, or whether the goal should be ignored during a merge.

Pyori is a command-line utility. For help on the various ways it can be invoked, simply pass the `-h` or `--help` flag:

	pyori.py -h


Example Usage
-------------

Open a terminal and create a Pyori project called "My Goals" with the command:

	pyori.py --new-project -p "My Goals"

The `--new-project` option will auto-make the required directory structure. Inside the newly created `My Goals/goals` folder, create the following files:

**research_project.yaml**

```
name: Research Project
description: Research a topic of my choice and write a report.
```

**research_project.csv**

```
task, score, done
Define scope of project, 2, no
"Gather sources: books, journal articles, and online resources", 5, no
Write outline, 2, no
Generate figures, 3, no
Write draft of introduction, 3, no
Write draft of conclusion, 3, no
Write draft of body, 8, no
Make first-pass edits, 5, no
Make second-pass edits, 3, no
Polish final draft, 2, no
Submit final draft, 1, no
```

**calculator_v1.yaml**

```
name: Calculator Version 0.1
description: Create a simple calculator GUI written in Python.
```

**calculator_v1.csv**

```
task, score, done
Setup boilerplate git project, 1, no
Show GUI window with text widget for output, 3, no
"Create buttons: numbers, operations, enter, and clear", 2, no
Hook button events to calculator operations, 2, no
Display output in text widget, 2, no
Write tests, 3, no
Polish GUI ease-of-use, 2, no
Write documentation, 3, no
Bump project version to 0.1, 1, no
```

Then, to merge the tasks into a single list, run the command:

	pyori.py -p "My Goals" > merged_tasks.csv

The merging process is randomized, but will attempt to order the tasks in such a way that high scoring tasks are surrounded by low scoring tasks. Here's an example of the output:

**merged_tasks.csv**:

```
goal,task,score,done
Research Project,Define scope of project,2,no
Calculator Version 0.1,Setup boilerplate git project,1,no
Research Project,"Gather sources: books, journal articles, and online resources",5,no
Research Project,Write outline,2,no
Research Project,Generate figures,3,no
Calculator Version 0.1,Show GUI window with text widget for output,3,no
Research Project,Write draft of introduction,3,no
Research Project,Write draft of conclusion,3,no
Research Project,Write draft of body,8,no
Calculator Version 0.1,"Create buttons: numbers, operations, enter, and clear",2,no
Calculator Version 0.1,Hook button events to calculator operations,2,no
Calculator Version 0.1,Display output in text widget,2,no
Calculator Version 0.1,Write tests,3,no
Calculator Version 0.1,Polish GUI ease-of-use,2,no
Research Project,Make first-pass edits,5,no
Research Project,Make second-pass edits,3,no
Research Project,Polish final draft,2,no
Calculator Version 0.1,Write documentation,3,no
Research Project,Submit final draft,1,no
Research Project,COMPLETE,,
Calculator Version 0.1,Bump project version to 0.1,1,no
Calculator Version 0.1,COMPLETE,,
```


Interactive Shell
-----------------

You can further experiment by loading the Pyori *interactive shell* and trying different commands:

	pyori.py -ip "My goals"

```
Type help or ? to list commands. CTRL-C to quit.

pyori> help

Documented commands (type help <topic>):
========================================
archive  coeffs  help  list  merge  path  quit

pyori> help list
List all goals.
pyori> list

Goals
=====
Calculator Version 0.1
Research Project

pyori> help merge
Merge the goals into a single list. Optionally provide a file path to print the output.
pyori> merge output.csv
Merge complete.
pyori> quit
```


Importance Coefficients
-----------------------

It is possible to prioritize some goals over others by tweaking their *importance coefficients*. These coefficients are non-negative integers that represent the importance that you place on each of your various goals. By default, each goal is assigned an importance coefficient of 1, but you can increase a goal's priority relative to the other goals by upping this number. For example, giving the Research and Calculator projects coefficients of 1 and 10, respectively, would cause Calculator tasks to be 10 times more likely to come before Research Project tasks. It is possible to change the importance coefficients via the interactive shell.

```
pyori> help coeffs
Set importance coefficients for each goal.
pyori> coeffs
Goal "Calculator Version 0.1" [1]: 10
Goal "Research Project" [1]: 1
pyori> merge output.csv
Merge complete.
pyori> quit
```

Note: Assigning zero to a goal causes the merging algorithm to ignore the goal entirely.


Dependencies
------------

Goals can be made dependent on other goals if desired. This will cause the tasks of the dependent goal to be scheduled *after* the prerequisite goal is complete. Dependencies are listed in the *info* file for a goal. For example:

**calculator_v1.yaml**

```
name: Calculator Version 0.1
depends:
- research_project
- another_goal
```


Ignoring a Goal
---------------

There are two ways to ignore a goal. The first way is to assign an importance coefficient of zero. The second way is to set `ignore: true` in the goal's *info* file:

**calculator_v1.yaml**

```
name: Calculator Version 0.1
ignore: true
```


Author & License
----------------

Pyori is developed by [Robert Taylor](mailto:rtaylor@pyrunner.com) and made available under the MIT license.
