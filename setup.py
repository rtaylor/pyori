from setuptools import setup
from pyori import get_version


with open('README.md', 'r') as f:
    readme = f.read()

with open('LICENSE', 'r') as f:
    license = f.read()


setup(
    name='pyori',
    version=get_version(),
    description=(
        'Python script to combine two or more prioritized '
        'goal sets into a single TODO list.'
        ),
    long_description=readme,
    author='Robert Taylor',
    author_email='rtaylor@pyrunner.com',
    url='https://bitbucket.org/rtaylor/pyori',
    license=license,
    install_requires=['docopt'],
    py_modules=['pyori'],
    entry_points={
        'console_scripts': ['pyori = pyori:main'],
        },
    )
